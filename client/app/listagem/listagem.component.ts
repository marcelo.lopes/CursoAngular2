import {Component, Input} from '@angular/core';
import { Http } from '@angular/http';

@Component({
    moduleId: module.id,
    selector: 'listagem',
    templateUrl: './listagem.component.html'
})
export class listagemComponent {
    
    fotos: Array<Object> = [];
    constructor(http: Http) {
        var stream = http.get('v1/fotos')
            .map(res => res.json())
            .subscribe(resposta => {
                this.fotos = resposta;
            }, erro => console.log(erro));
    }
}