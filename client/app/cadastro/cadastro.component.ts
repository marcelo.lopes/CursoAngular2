import { Component, Input } from '@angular/core';
import { FotoComponent } from '../foto/foto.component'
import { Http, Headers } from '@angular/http';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    moduleId: module.id,
    selector: 'cadastro',
    templateUrl: './cadastro.component.html'
})
export class CadastroComponent {

    @Input() foto: FotoComponent = new FotoComponent();
    meuForm:  FormGroup;

    constructor(private http: Http, private fb : FormBuilder) {
        this.meuForm = fb.group({
            titulo: ['', 
                        Validators.compose([Validators.required, Validators.minLength(5)])
                    ],
            url: ['',Validators.required],
            descricao: []
        })
    }

    cadastrar(event) {
        event.preventDefault();
        var hd = new Headers();
        hd.append('Content-Type', 'application/json');
        this.http.post('v1/fotos', JSON.stringify(this.foto), { headers: hd })
            .subscribe(() => {
                this.foto = new FotoComponent();
                console.log('salvo');
            });
    }

}