import { Pipe, PipeTransform } from '@angular/core';
import {FotoComponent} from './foto.component'

@Pipe({
    name: "filtroPorTitulo"
})
export class filtroPorTitulo implements PipeTransform {

    transform(fotos : FotoComponent[], difitados : string) {
        return fotos.filter( foto => foto.titulo.toLowerCase().includes(difitados.toLocaleLowerCase()))
    }
}